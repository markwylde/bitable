$(function() {
	var elementsLoading = 0;
	
	$('[data-auto-load]').each( function() {
		var self = this;
		
		$.ajax( { url: $(this).attr('data-auto-load'), 
				  async: false,
				  success: function(msg) {
					$(self).html(msg);
				}
		});
	});

});
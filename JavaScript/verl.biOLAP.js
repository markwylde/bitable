/*
	biOLAP is part of the vBI Toolkit.
*/

(function($){
	$.widget("v.biOLAP", {
		version: "1.0.2",
		
			options: {
				data: null
			},
			
		_create: function() {
		
				var newHead = $('> thead', this.element).length ? $('> thead', this.element) : $('<thead><tr>').appendTo(this.element);
				var newBody = $('> tbody', this.element).length ? $('> tbody', this.element) : $('<tbody>').appendTo(this.element);
				var newFoot = $('> tfoot', this.element).length ? $('> tfoot', this.element) : $('<tfoot>').appendTo(this.element);
		
				var self = this;

				var data = JSON.parse(this.options.data);

				var index = 0;
				var level = 0;
				var addedTitles = [];
				var filteredData = [];
				var headers = [];

				function searchStringInArray(str, strArray) {
					for (var j = 0; j < strArray.length; j++) {
						if (strArray[j] == str) return j;
					}
					return -1;
				}

				var arrayElements = [];
				var val;
				var match;
				var index = 0;
				var column = 0;

				// NOTE: *May* be able to make the following two functions into one, halving the execution speed.
				function makeArrayReadable(data, parentName) {
					if (typeof data === 'string') {
						return;
					}

					$.each(data, function (i, v) {
						if (typeof v === 'string') {
							return;
						}

						// Headers
						for (strName in v) {
							if (v[strName] instanceof Array) { // We've hit an array
								var data = v[strName];

								if (parentName) {
									strName = parentName + "." + strName;
								}

								makeArrayReadable(data, strName);
							} else if (typeof v[strName] === 'object') {
								if (parentName) {
									strName = parentName + "." + strName;
								}

								makeArrayReadable(v, strName);
							} else {
								// Add to headers
								if (parentName) {
									strName = parentName + "." + strName;
								}

								var match = searchStringInArray(strName, headers);

								if (match == -1) {
									headers.push(strName);
								}
							}
						}
					});
				}
				var overrides = [];

				function orderArray(data, index, parentName, previousArray) {
					if (typeof data === 'string') {
						return;
					}
					$.each(data, function (i, v) {
						var extraRows = 0;

						for (strName in v) {
							if (typeof data === 'string') {
								return;
							}

							if (v[strName] instanceof Array) {
								extraRows = v[strName].length - 1;

								if (parentName) {
									strName = parentName + "." + strName;
								}

								var previous = (previous ? null : arrayElements[index]);

								orderArray(v[strName], index, strName, previous);
							} else if (typeof v[strName] === 'object') {
								if (parentName) {
									strName = parentName + "." + strName;
								}

								orderArray(v, index, strName);
							} else {
								if (!arrayElements[index]) {
									arrayElements[index] = [];
								}

								var fieldName = strName;

								if (parentName) {
									fieldName = parentName + "." + fieldName;
								}

								match = searchStringInArray(fieldName, headers);

								if (previousArray && !overrides[index]) {
									$.each(previousArray, function (o, z) {

										// handle the array overrides from previous row
										if (z && z.data && z.data.length) {
											
											arrayElements[index][z.column] = z;
										}
									});

									overrides[index] = true;
								}

								if (match > -1) {
									arrayElements[index][match] = { data: v[strName], column: match };
								}
							}
						}

						index += (extraRows + 1);
					});
				}

				function addBodyRows(data) {
					// Loop through array items
					$.each(arrayElements, function (i, v) {
						var bodyTr = $("<tr>");
						// each of these rows is a <tr>
						for (var i = 0; i < headers.length; i++) {
							bodyTr.append($("<td>"));
						}

						$.each(v, function (o, z) {
							if (z && z.data && z.data.length) {
								bodyTr.find("td").eq(z.column).text(z.data);
							}
						});

						$($(self.element).find('tbody').append(bodyTr));
					});
				}

				makeArrayReadable(data);

				orderArray(data, 0);

				// Create all the headers.
				for (i = 0; i < headers.length; i++) {

					$($(self.element).find('thead tr').append($("<th>").text((headers[i]))));
				}

				addBodyRows(data);


		}
	});


}(jQuery));

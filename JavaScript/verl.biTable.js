/*
	biTable is part of the vBI Toolkit.
*/

var elDragged;

(function($){
	$.widget("v.biTable", {
		version: "1.0.1",
		lastRefreshTime: null,
		
		dataJSON: null,
			
		options: {
			dataSourceName: null,
			defaultField: null
		},
		
		_create: function() {
			var self = this;
			
			$(this.element).bind("biTableRefresh", this.options.biTableRefresh);
			
			if (!$(self.element).find("table").length) {
				$(self.element).append('<table data-source="' + self.options.dataSourceName + '"><thead><tr><th data-type="text" data-formula="' + self.options.defaultField + '">' + self.options.defaultField + '</th></tr><tr class="format"><td></td></tr></thead></table>');
			} else {
				if (!self.options.dataSourceName) { self.options.dataSourceName = $("table", self.element).attr("data-source"); } 
			}

			$(self.element).addClass("biTable");
			
			self.dataJSON = new Array();

			// ----------------------------------------------
			// Put the table into a JSON array
			// ----------------------------------------------
			$(dataSources[this.options.dataSourceName]).find("tbody tr").each( function() {
				
				var currentRecord = new Object();
			
				$(this).find("td").each( function() {
					var $th = $(this).closest('table').find('th').eq($(this).index());
					currentRecord[$th.text()] = $(this).text();
				});
				
				self.dataJSON.push(currentRecord);
			});
			
			$(self.element).draggable({'cancel': 'table', opacity: 0.7,  onBeforeDrag: function(e){ console.log(e); }});
			$(self.element).css("position","absolute");
			$("table", self.element).dragtable();
			
			// ----------------------------------------------
			// Manage the drag and drop of fields
			// ----------------------------------------------

			// # Add the edges to the table headers for dropping
			$(self.element).find("table").append("<tbody></tbody>");

			$(self.element).find("thead th").each( function() {
				$(this).html( "<span class='select left' style='display: none;'></span><p>" + $(this).html() + "</p><span class='select right' style='display: none;'></span>" );
			});
			

			// # Stop dragging
			$(self.element).on("mouseover", "span.select", function(e) {
				if (!elDragged) { return false; }
				$(this).addClass("hover");
			});
			$(self.element).on("mouseleave", "span.select", function(e) {
				
				$(this).removeClass("hover");
			});
			
			$(self.element).on("mouseup", "span.select", function(e) {
				if (!elDragged) { return false; }

				if (self.options.dataSourceName == $(elDragged).attr("data-source")) {

					columnPosition = $(this).parent().index();

					if ( $(this).hasClass('left') ) {
						$(this).parents('table').find('.format td').eq(columnPosition).before("<td></td>");
						$(this).parents('table').find('th').eq(columnPosition).before("<th data-type='text' data-formula='" + $(elDragged).attr("data-formula") + "'><span class='select left'></span><p>" + $(elDragged).html() + "</p><span class='select right'></span></th>");
					} else {
						$(this).parents('table').find('.format td').eq(columnPosition).after("<td></td>");
						$(this).parents('table').find('th').eq(columnPosition).after("<th data-type='text' data-formula='" + $(elDragged).attr("data-formula") + "'><span class='select left'></span><p>" + $(elDragged).html() + "</p><span class='select right'></span></th>");
					}
				}
				
				$("*").css("cursor", "");
				$('span.select').removeClass('available');
				$('span.select').hide();
				
				elDragged = null;
				self.refreshData();
			});
			
				
			// ----------------------------------------------
			// Manage the right click context menu
			// ----------------------------------------------
			$('.content').on('contextmenu', 'div.biTable', function(event) {
					if (selectedColumn != null) $('.selectedColumnOnly').show(); else $('.selectedColumnOnly').hide();
					
					$('#biMenuTable').css('top', event.pageY);
					$('#biMenuTable').css('left', event.pageX);
					$('#biMenuTable').show();
					event.preventDefault();
					
					selectedTable = this;
			});
			$('.biContextMenu').on('click', function() { $(".biContextMenu").hide(); });
			
			// ----------------------------------------------
					
					
			this.refreshData();
		},
		refreshData: function() {
		
			var self = this;
			
			$(self.element).find("tbody").html("");

			$(self.dataJSON).each( function() {
			
				currentRowData = this;
				newRow = $('<tr>');

			
				$(self.element).find("thead th").each( function() {
					if (typeof $(this).attr("data-type") == 'undefined' || $(this).attr("data-type") == "" ) $(this).attr("data-type", "text");
				
					// Apply any formatting?
					if ($(this).attr("data-format")) {
					
						if ($(this).attr("data-format")) {
								if ( $(this).attr("data-type") == "date" ) {
									theValue = moment(currentRowData[$(this).attr("data-formula")]).format($(this).attr("data-format"));
								} else if ( $(this).attr("data-type") == "number" ) {
									if (isNumber(currentRowData[$(this).attr("data-formula")])) {
										theValue = numeral(currentRowData[$(this).attr("data-formula")]).format($(this).attr("data-format"));
									} else {
										theValue = "[[ Wrong Data Type ]]";
									}
								} else {
									theValue = currentRowData[$(this).attr("data-formula")];
								}

						} else {
							theValue = currentRowData[$(this).attr("data-formula")];
						}
					} else {
							theValue = currentRowData[$(this).attr("data-formula")];
						}
					
					newCell = $("<td>").html( theValue );
										
					$(newCell).attr("style", $("thead .format td", self.element).eq($(this).index()).attr("style"));
					
					if ($(this).hasClass("selected")) newCell.addClass("selected");

					$(newCell).attr("data-value", currentRowData[$(this).attr("data-formula")] );
					
					newCell.addClass("allowSelect");
					
					newRow.append(newCell);
				});

				
				$(self.element).find("tbody").append(newRow);

				// ------------------------------
				// REMOVE DUPLICATE ROWS
				// ------------------------------
				var seen = {};
				$(self.element).find("tbody tr").each( function() { 
					var txt = $(this).text();
					if (seen[txt])
						$(this).remove();
					else
						seen[txt] = true;
				});
				// ------------------------------

			});

			self.lastRefreshTime = new Date();
			$('[data-label="lastRefreshDate"]').text(moment(self.lastRefreshTime).format("MMMM Do YYYY, h:mm:ss a"));
			$('[data-label="lastRefreshTimeago"]').text(moment(self.lastRefreshTime).fromNow());
			
		}
});


// Add the defined functions to the jQuery Widget
$.extend($.v.biTable.prototype, ({v: function() { 
	var self = this; 

		/*********************************************************
			Initialise the Core Table Methods
		**********************************************************/
		biTableFunctions = new Object();

		// -------------------------------------------------------
		// # COLUMN METHODS
		// -------------------------------------------------------
		biTableFunctions.dataSource = function() {
			return $(self.element).find("table").attr("data-source");
		}
		
		// biTable().columns(x)
		biTableFunctions.columns = function(columnNumber) {
			var currentColumn = columnNumber;
			if (columnNumber == 'selected') columnNumber = selectedColumn;

			var currentColumnHead = $(self.element).find('thead>tr>th:nth-child(' + (columnNumber + 1) + ')');
			var currentColumnBody = $(self.element).find('tbody>tr>td:nth-child(' + (columnNumber + 1) + ')');
			
			columns = new Object();
			
			// COLUMN: Properties
			columns.header = new Object();
			columns.header.text = function(newValue) {
				if( typeof newValue !== 'undefined' ) {
					currentColumnHead.find("p").html(newValue);
					return true;
				} else {
					return currentColumnHead.find("p").html();
				}
			}
			columns.formula = function(newValue) {
				if( typeof newValue !== 'undefined' ) {
					currentColumnHead.attr("data-formula", newValue);
					self.refreshData();
					return true;
				} else {
					return currentColumnHead.attr("data-formula");
				}
			}
			columns.type = function(newValue) {
				if( typeof newValue !== 'undefined' ) {
					currentColumnHead.attr("data-type", newValue);
					self.refreshData();
					return true;
				} else {
					return currentColumnHead.attr("data-type");
				}
			}
			columns.format = function(newValue) {
				if( typeof newValue !== 'undefined' ) {
					currentColumnHead.attr("data-format", newValue);
					self.refreshData();
					return true;
				} else {
					return currentColumnHead.attr("data-format");
				}
			}
			
			// COLUMN: Methods
			columns.remove = function() {
				// Remove the header
				$(self.element).find('thead>tr>th:nth-child(' + (columnNumber + 1) + ')').remove();
				// Remove the format header row
				$(self.element).find('thead>tr>td:nth-child(' + (columnNumber + 1) + ')').remove();
				
				if (!$(self.element).find("th").length) $(self.element).remove();
				self.refreshData();
				clearSelection();
			}
			columns.select = function() {
				clearSelection();
				
				// Set the global variables to hard select
				selectedTable = $(self.element);
				selectedColumn = columnNumber;
				selectedColumnHead = currentColumnHead;
				selectedColumnBody = currentColumnBody;
				
				
				// Pretty display and highlights
				$(".biTable").find('.selected').removeClass('selected');
				
				$(self.element).find('tbody>tr>td:nth-child(' + (columnNumber + 1) + ')').addClass("selected");
				$(self.element).parents('table').addClass('selected');
				$(selectedColumnHead).addClass('selected');

				if (selectedColumn != null) $('.selectedColumnOnly').show(); else $('.selectedColumnOnly').hide() ;

				$(document).trigger("onSelectionChange");
			}

			return columns;

		};
		// selectedColumn is a direct mapping to [columns]
		biTableFunctions.selectedColumn = partial(biTableFunctions.columns, "selected");
	
	return(biTableFunctions); 
}}));

}(jQuery));